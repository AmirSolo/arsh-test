## Getting Started

This app uses [Google Custom Search API](https://developers.google.com/custom-search/v1/using_rest).

So in order to run the app locally first you need to get a google API key from [here](https://developers.google.com/custom-search/v1/introduction#identify_your_application_to_google_with_api_key).

Then rename `.env.example` to `.env.local` and update the value to your own key.

Install dependecies:

```bash
npm install
# or
yarn install
```

Run the development server:

```bash
npm run dev
# or
yarn dev
```

Open [http://localhost:3000](http://localhost:3000) with your browser to see the result.

> There is also a live version of the app available at [https://arsh.amirsolo.com](https://arsh.amirsolo.com)
