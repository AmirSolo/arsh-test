import { useState, useEffect } from 'react'

// Redux
import { useSelector, useDispatch } from 'react-redux'
import {
  searchGoogleStart,
  clearSearchResult
} from '../../redux/search/search.actions'

// components
import Spinner from '../Spinner/Spinner'
import SearchResult from './SearchResult/SearchResult'
import SearchErrors from './SearchErrors/SearchErrors'

// Styled-components
import { SearchContainer, Title, Input } from './index.styles'

function Search() {
  const dispatch = useDispatch()
  const isFetching = useSelector((state) => state.search.isFetching)
  const [inputValue, setInputValue] = useState('')

  useEffect(() => {
    const searchQuery = inputValue.trim()

    // Clear search result on empty input
    if (searchQuery.length === 0) return dispatch(clearSearchResult())

    dispatch(searchGoogleStart(searchQuery))
  }, [inputValue])

  return (
    <SearchContainer>
      <Title>Google Custom Search API</Title>

      {/* Search input */}
      <Input
        type='text'
        placeholder='Search something...'
        value={inputValue}
        onChange={(e) => setInputValue(e.target.value)}
      />

      {/* show errors coming from the API  */}
      <SearchErrors />

      {/* Loading state */}
      <Spinner isFetching={isFetching} size={40} />

      {/* Data (search result) coming from the API */}
      <SearchResult />
    </SearchContainer>
  )
}

export default Search
