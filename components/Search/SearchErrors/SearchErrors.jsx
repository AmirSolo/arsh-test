import React from 'react'

import { connect, useSelector } from 'react-redux'

// Styled Components
import { ErrorsContainer, ErrorItem } from './SearchErrors.styles'

const SearchErrors = () => {
  const error = useSelector((state) => state.search.error)

  return (
    <ErrorsContainer>
      <ErrorItem>{error && error.message}</ErrorItem>
    </ErrorsContainer>
  )
}

export default SearchErrors
