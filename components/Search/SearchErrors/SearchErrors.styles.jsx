import styled from 'styled-components'

export const ErrorsContainer = styled.div`
  width: 100%;
  margin-top: 10px;
`
export const ErrorItem = styled.div`
  width: 100%;
  display: block;
  color: red;
`
