import React from 'react'
import PropTypes from 'prop-types'

// Styled-components
import {
  ListItem,
  CustomLink,
  DisplayedLink,
  LinkTitle,
  Snippet
} from './SearchResultItem.styles'

// External Link Component
const ExternalLink = ({
  className,
  target = '_self',
  href,
  children,
  ...props
}) => (
  <CustomLink
    className={`${className && className}`}
    target={target}
    rel='noopener noreferrer'
    href={href}
    {...props}
  >
    {children}
  </CustomLink>
)

const SearchResultItem = ({ item }) => {
  const { title, link, displayLink, snippet } = item
  return (
    <ListItem>
      <ExternalLink href={link}>
        <DisplayedLink>{displayLink}</DisplayedLink>
        <LinkTitle>{title}</LinkTitle>
      </ExternalLink>
      <Snippet>{snippet}</Snippet>
    </ListItem>
  )
}

SearchResultItem.propTypes = {
  item: PropTypes.object.isRequired
}

export default SearchResultItem
