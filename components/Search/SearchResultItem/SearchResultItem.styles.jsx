import styled from 'styled-components'

export const ListItem = styled.li`
  width: 100%;
  margin-bottom: 2.2rem;
`

export const LinkTitle = styled.h6`
  display: block;
  color: #1a0dab;
  font-size: 1.4rem;
  padding: 5px 0;
  font-weight: 400;

  @media screen and (max-width: 768px) {
    font-size: 1.2rem;
  }
`

export const CustomLink = styled.a`
  display: block;
  width: 100%;
  border-radius: 5px;
  &:hover ${LinkTitle} {
    text-decoration: underline;
  }
`
export const DisplayedLink = styled.span`
  display: block;
  color: #333;
  font-size: 0.92rem;
  font-weight: 500;
`

export const Snippet = styled.p`
  color: #666;
  line-height: 1.4rem;
  font-size: 0.98rem;

  @media screen and (max-width: 768px) {
    font-size: 0.95rem;
  }
`
