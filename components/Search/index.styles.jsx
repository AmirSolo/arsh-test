import styled from 'styled-components'

export const SearchContainer = styled.div`
  width: 700px;
  display: flex;
  flex-direction: column;
  align-items: center;
  background-color: #f9f9f9;
  border: 1px solid #eee;
  border-radius: 10px;
  margin: 1rem;
  padding: 20px 40px;
  box-shadow: 0 1.6px 10px rgba(0, 0, 0, 0.01), 0 13px 80px rgba(0, 0, 0, 0.02);

  @media screen and (max-width: 768px) {
    width: 90%;
  }
`

export const Title = styled.h1`
  color: #555;
  margin-bottom: 1.5rem;

  @media (max-width: 768px) {
    font-size: 1.2rem;
    text-align: center;
  }
`

export const Input = styled.input`
  font-size: 1.2rem;
  color: #333;
  border: 1px solid #aaa;
  padding: 12px 10px;
  border-radius: 5px;
  width: 100%;
`
