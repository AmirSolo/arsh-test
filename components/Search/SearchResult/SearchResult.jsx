import React from 'react'

import { useSelector } from 'react-redux'

// components
import SearchListItem from '../SearchResultItem/SearchResultItem'

// Styled-components
import { SearchResultContainer, PageTitle, List } from './SearchResult.styles'

const SearchResult = () => {
  const searchResult = useSelector((state) => state.search.searchResult)

  return (
    <SearchResultContainer>
      <PageTitle>Search Result: </PageTitle>
      <List>
        {searchResult?.length > 0 &&
          searchResult.map((item, ind) => (
            <SearchListItem key={ind} item={item} />
          ))}
      </List>
    </SearchResultContainer>
  )
}

export default SearchResult
