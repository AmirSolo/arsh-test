import styled from 'styled-components'

export const SearchResultContainer = styled.div`
  width: 100%;
  display: flex;
  flex-direction: column;
  align-items: center;
`

export const PageTitle = styled.h1`
  color: #555;
  text-align: left;
  width: 100%;
  margin-bottom: 2rem;

  @media screen and (max-width: 768px) {
    font-size: 1.2rem;
    text-align: center;
  }
`
export const List = styled.ul`
  width: 100%;
  list-style: none;
`
