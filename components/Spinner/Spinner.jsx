import React from 'react'
import PropTypes from 'prop-types'

// Styled-component
import { SpinnerContainer, SpinnerWrapper } from './Spinner.styles'

const Spinner = ({ isFetching, size = 30, color = '#666666', style = {} }) => {
  const pxSize = size + 'px'
  return (
    <SpinnerWrapper>
      {isFetching && (
        <SpinnerContainer size={pxSize} color={color} style={style} />
      )}
    </SpinnerWrapper>
  )
}

Spinner.propTypes = {
  size: PropTypes.number,
  color: PropTypes.string,
  style: PropTypes.object
}

export default Spinner
