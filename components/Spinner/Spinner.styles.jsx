import styled from 'styled-components'

export const SpinnerWrapper = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;
  width: 100%;
  min-height: 100px;
`

export const SpinnerContainer = styled.div`
  display: inline-block;
  position: absolute;
  right: 0;
  left: 0;
  margin: auto;
  width: ${(p) => p.size};
  height: ${(p) => p.size};
  border: 3px solid rgba(195, 195, 195, 0.6);
  border-radius: 50%;
  border-top-color: ${(p) => p.color};
  animation: spin 1s ease-in-out infinite;
  -webkit-animation: spin 1s ease-in-out infinite;

  @keyframes spin {
    to {
      -webkit-transform: rotate(360deg);
    }
  }
  @-webkit-keyframes spin {
    to {
      -webkit-transform: rotate(360deg);
    }
  }
`
