import { createStore, applyMiddleware } from 'redux'
import { composeWithDevTools } from 'redux-devtools-extension'
import { createWrapper } from 'next-redux-wrapper'
import createSagaMiddleware from 'redux-saga'
import rootReducer from './rootReducer'
import rootSaga from './rootSaga'

export const makeStore = (context) => {
  const sagaMiddleware = createSagaMiddleware()

  // compose redux with devtools extension
  const enhancedComposer = composeWithDevTools({ trace: true })

  // make store
  const store = createStore(
    rootReducer,
    enhancedComposer(applyMiddleware(sagaMiddleware))
  )

  // Run sagas
  sagaMiddleware.run(rootSaga)

  return store
}

// export an assembled wrapper
export const wrapper = createWrapper(makeStore)
