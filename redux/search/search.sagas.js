import {
  takeLatest,
  put,
  all,
  call,
  delay,
  take,
  race
} from 'redux-saga/effects'

import { SEARCH_REQUESTED, CLEAR_SEARCH_RESULT } from './search.types'
import { searchGoogleSuccess, searchGoogleFailure } from './search.actions'

//  Sign out user helper
export function* searchGoogle({ payload: searchQuery }) {
  try {
    // delay each request for 2 seconds (avoid google rate limiter which is 1.5s query per second)
    yield delay(2000)

    // Fetch data from search api
    const response = yield fetch('/api/search-google', {
      method: 'POST',
      cache: 'no-cache',
      credentials: 'same-origin',
      headers: {
        'Content-Type': 'application/json'
      },
      body: JSON.stringify({ searchQuery })
    })
    const data = yield response.json()

    // If there was some api error
    if (data.error) {
      const { code, message } = data.error
      return yield put(searchGoogleFailure({ code, message }))
    }

    yield put(searchGoogleSuccess(data.items))
  } catch (error) {
    yield put(searchGoogleFailure(error))
  }
}

export function* onSearchStart() {
  // call worker saga on "search request" AND cancell the worker when "clearing search result"
  yield takeLatest(SEARCH_REQUESTED, function* (...args) {
    yield race({
      task: call(searchGoogle, ...args),
      cancel: take(CLEAR_SEARCH_RESULT)
    })
  })
}

export function* searchSagas() {
  yield all([call(onSearchStart)])
}
