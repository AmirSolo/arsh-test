import {
  SEARCH_REQUESTED,
  SEARCH_SUCCEDED,
  SEARCH_FAILED,
  CLEAR_SEARCH_RESULT
} from './search.types'

const INIT_STATE = {
  isFetching: false,
  searchQuery: '',
  searchResult: [],
  error: null
}

const searchReducer = (state = INIT_STATE, action) => {
  const { type, payload } = action

  switch (type) {
    case SEARCH_REQUESTED:
      return {
        ...state,
        isFetching: true,
        searchQuery: payload
      }
    case SEARCH_SUCCEDED:
      return {
        ...state,
        isFetching: false,
        searchResult: [...payload]
      }
    case SEARCH_FAILED:
      return {
        ...state,
        isFetching: false,
        error: payload
      }
    case CLEAR_SEARCH_RESULT:
      return {
        ...state,
        searchResult: [],
        isFetching: false
      }
    default:
      return state
  }
}

export default searchReducer
