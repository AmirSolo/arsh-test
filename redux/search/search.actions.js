import {
  SEARCH_REQUESTED,
  SEARCH_SUCCEDED,
  SEARCH_FAILED,
  CLEAR_SEARCH_RESULT
} from './search.types'

export const searchGoogleStart = (searchQuery) => ({
  type: SEARCH_REQUESTED,
  payload: searchQuery
})

export const searchGoogleSuccess = (searchResult) => ({
  type: SEARCH_SUCCEDED,
  payload: searchResult
})

export const searchGoogleFailure = (error) => ({
  type: SEARCH_FAILED,
  payload: error
})
export const clearSearchResult = () => ({
  type: CLEAR_SEARCH_RESULT
})
