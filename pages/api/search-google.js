import { fetchDataFromGoogle } from '../../utils/googleApi'

const sendResponseError = (
  res,
  errorCode = 500,
  errorMessage = 'Server Error!'
) => {
  res.status(500).json({ error: { code: errorCode, message: errorMessage } })
}

const handler = async (req, res) => {
  if (req.method === 'POST') {
    try {
      // Fetch data from google custom search api
      const data = await fetchDataFromGoogle(req.body.searchQuery)

      return res.status(200).json(data)
    } catch (error) {
      return sendResponseError(res, 500, error.message)
    }
  } else {
    return sendResponseError(
      res,
      500,
      `Send a POST request with the 'searchQuery' field in the request body.`
    )
  }
}

export default handler
