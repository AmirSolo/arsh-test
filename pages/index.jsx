// components
import Search from '../components/Search'

function Home() {
  return (
    <div className='container'>
      <Search />
    </div>
  )
}

export default Home
