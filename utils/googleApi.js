const fetchDataFromGoogle = async (searchQuery) => {
  const googleApiKey = encodeURI(process.env.GOOGLE_API_KEY)

  const baseUrl = 'https://www.googleapis.com/customsearch/v1'
  const url = `${baseUrl}?key=${googleApiKey}&cx=cb8faab1a2e75a88c&q=${encodeURI(
    searchQuery
  )}`

  // Fetch data
  const res = await fetch(url)
  const data = await res.json()

  return data
}

export { fetchDataFromGoogle }
